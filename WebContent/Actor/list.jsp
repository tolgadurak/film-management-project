<%@page import="java.util.Locale"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tolga.jss.model.Actor"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	@SuppressWarnings("unchecked")
	List<Actor> actors = (ArrayList<Actor>) request.getAttribute("actors");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<table>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Gender</th>
			<th>Birth Date</th>
			<th>Rating</th>
			<th>Biography</th>
		</tr>
		<%
			for (Actor actor : actors) {
		%>
		<tr>
			<td><%=actor.getFirstName()%></td>
			<td><%=actor.getLastName()%></td>
			<td><%=actor.getGender()%></td>
			<td><%=actor.getPresentationBirthDate()%></td>
			<td><%=actor.getRating()%>%</td>
			<td><%=actor.getBiography()%></td>
			<td><a href="ActorServlet?action=details&id=<%=actor.getId()%>">Details</a></td>
			<td><a href="ActorServlet?action=edit&id=<%=actor.getId()%>">Edit</a></td>
			<td><a href="ActorServlet?action=delete&id=<%=actor.getId()%>">Delete</a></td>
		</tr>
		<%
			}
		%>
	</table>
	<div>
		<a href="ActorServlet?action=create"
			title="Click here to insert new actor">Create New</a>
	</div>
	<a href="index.jsp">Go index page</a>
</body>
</html>