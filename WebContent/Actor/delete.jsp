<%@page import="com.tolga.jss.model.Actor"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Actor actor = (Actor) request.getAttribute("actor");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Delete</title>
</head>
<body>
	<h1>Delete</h1>
	<h2>Are you sure you want to delete this?</h2>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<form action="ActorServlet" method="post">
		<input type="hidden" name="id" value="<%=actor.getId()%>" /> <input
			type="hidden" name="action" value="delete_item" />
		<table>
			<tr>
				<td>First Name:</td>
				<td><%=actor.getFirstName()%></td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td><%=actor.getLastName()%></td>
			</tr>
			<tr>
				<td>Gender:</td>
				<td><%=actor.getGender()%></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
		<a href="ActorServlet?action=list">Back to list</a> <input
			type="submit" value="Delete" />
	</form>
	<a href="index.jsp">Go index page</a>
</body>
</html>