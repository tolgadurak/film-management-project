<%@page import="com.tolga.jss.model.Actor"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Actor actor = (Actor) request.getAttribute("actor");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<dl>
		<dt>First Name:</dt>
		<dd><%=actor.getFirstName()%></dd>
		<dt>Last Name:</dt>
		<dd><%=actor.getLastName()%></dd>
		<dt>Gender:</dt>
		<dd><%=actor.getGender()%></dd>
		<dt>Birth Date:</dt>
		<dd><%=actor.getPresentationBirthDate()%></dd>
		<dt>Rating:</dt>
		<dd><%=actor.getRating()%></dd>
		<dt>Biography:</dt>
		<dd><%=actor.getBiography()%></dd>
		<dt></dt>
		<dd>
			<a href="ActorServlet?action=list">Back to list</a>
		</dd>
		<dt></dt>
		<dd>
			<a href="index.jsp">Go index page</a>
		</dd>
	</dl>
</body>
</html>