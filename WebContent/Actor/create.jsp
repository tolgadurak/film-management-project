<%@page import="com.tolga.jss.model.Actor"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE>
<%
	Actor actor = (Actor) request.getAttribute("actor");
	String username = (String) session.getAttribute("username");
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1>
		<%=request.getAttribute("page_text")%>
	</h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<form action="ActorServlet" method="post">
		<table>
			<tr>
				<td>First Name:</td>
				<td><input type="text" name="first_name" /></td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td><input type="text" name="last_name" /></td>
			</tr>
			<tr>
				<td>Gender:</td>
				<td><select name="gender">
						<option value="">- Please Select</option>
						<option value="M">Male</option>
						<option value="F">Female</option>
				</select></td>
			</tr>
			<tr>
				<td>Birth Date:</td>
				<td><input type="text" name="birth_date" /></td>
			</tr>
			<tr>
				<td>Rating:</td>
				<td><input type="number" name="rating" /></td>
			</tr>
			<tr>
				<td>Biography:</td>
				<td><textarea name="biography"></textarea></td>
			</tr>
			<tr>
				<td class="label">&nbsp;</td>
				<td><input type="submit" value="Save Actor" /> <input
					type="hidden" name="id" value="<%=actor.getId()%>" /> <input
					type="hidden" name="action" value="save_edit" /></td>

			</tr>
		</table>
		<a href="index.jsp">Go index page</a>
	</form>

</body>
</html>