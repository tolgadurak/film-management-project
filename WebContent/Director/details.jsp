<%@page import="com.tolga.jss.model.Director"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Director director = (Director) request.getAttribute("director");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<dl>
		<dt>First Name:</dt>
		<dd><%=director.getFirstName()%></dd>
		<dt>Last Name:</dt>
		<dd><%=director.getLastName()%></dd>
		<dt>Gender:</dt>
		<dd><%=director.getGender()%></dd>
		<dt>Birth Date:</dt>
		<dd><%=director.getPresentationBirthDate()%></dd>
		<dt></dt>
		<dd>
			<a href="DirectorServlet?action=list">Back to list</a>
		</dd>
	</dl>
	<a href="index.jsp">Go index page</a>
</body>
</html>