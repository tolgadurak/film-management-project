<%@page import="java.util.ArrayList"%>
<%@page import="com.tolga.jss.model.Director"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	@SuppressWarnings("unchecked")
	List<Director> directors = (ArrayList<Director>) request.getAttribute("directors");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<table>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Gender</th>
			<th>Birth Date</th>
		</tr>
		<%
			for (Director director : directors) {
		%>
		<tr>
			<td><%=director.getFirstName()%></td>
			<td><%=director.getLastName()%></td>
			<td><%=director.getGender()%></td>
			<td><%=director.getPresentationBirthDate()%></td>
			<td><a
				href="DirectorServlet?action=details&id=<%=director.getId()%>">Details</a></td>
			<td><a
				href="DirectorServlet?action=edit&id=<%=director.getId()%>">Edit</a></td>
			<td><a
				href="DirectorServlet?action=delete&id=<%=director.getId()%>">Delete</a></td>
		</tr>
		<%
			}
		%>
	</table>
	<div>
		<a href="DirectorServlet?action=create"
			title="Click here to insert new director">Create New</a>
	</div>
	<a href="index.jsp">Go index page</a>
</body>
</html>