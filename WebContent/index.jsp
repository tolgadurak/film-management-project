<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%}else { %>
		You are not logged in, <a href="login.jsp"> Go to login page</a>
		<%} %>

	</div>
	<div>
		<ul>
			<li><a href="ActorServlet?action=list">List Actors</a></li>
			<li><a href="DirectorServlet?action=list">List Directors</a></li>
			<li><a href="GenreServlet?action=list">List Genres</a></li>
			<li><a href="MovieServlet?action=list">List Movies</a></li>
		</ul>
	</div>

</body>
</html>