<%@page import="com.tolga.jss.model.Director"%>
<%@page import="com.tolga.jss.model.Genre"%>
<%@page import="com.tolga.jss.model.Actor"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.tolga.jss.model.Movie"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
	Movie movie = (Movie) request.getAttribute("movie");
	List<Integer> actorsIds = (ArrayList<Integer>) movie.getActorIds();
	List<Integer> genresIds = (ArrayList<Integer>) movie.getGenresIds();
	@SuppressWarnings("unchecked")
	List<Director> directorList = (ArrayList<Director>) request.getAttribute("directors");
	@SuppressWarnings("unchecked")
	List<Actor> actorsList = (ArrayList<Actor>) request.getAttribute("actors");
	@SuppressWarnings("unchecked")
	List<Genre> genresList = (ArrayList<Genre>) request.getAttribute("genres");
	String username = (String) session.getAttribute("username");
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>
		<%=request.getAttribute("page_text")%>
	</h1>
	<div>
		<%
			if (username != null) {
		%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%
			}
		%>
	</div>
	<form action="MovieServlet" method="post">
		<table>
			<tr>
				<td>Movie Title:</td>
				<td><input type="text" name="movie_title"
					value="<%=movie.getTitle()%>" /></td>
			</tr>
			<tr>
				<td>Movie Year:</td>
				<td><input type="text" name="movie_year"
					value="<%=movie.getYear()%>" /></td>
			</tr>
			<tr>
				<td>Rating:</td>
				<td><input type="text" name="movie_rating"
					value="<%=movie.getRating()%>" /></td>
			</tr>
			<tr>
				<td>Description:</td>
				<td><textarea name="movie_description"><%=movie.getDescription()%></textarea></td>
			</tr>
			<tr>
				<td>Director Name:</td>
				<td><select name="movie_director">
						<%
							for (Director director : directorList) {
						%>
						<option value="<%=director.getId()%>"><%=director.getFirstName() + " " + director.getLastName()%></option>
						<%
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Actors:</td>
				<td>
					<%
						for (Actor actor : actorsList) {
					%> <input type="checkbox"
					<%=actorsIds.contains(actor.getId()) ? "checked" : ""%>
					name="actors[]" value="<%=actor.getId()%>"><%=actor.getFirstName() + " " + actor.getLastName()%><br />
					<%
						}
					%>
				</td>
			</tr>
			<tr>
				<td>Genre:</td>
				<td>
					<%
						for (Genre genre : genresList) {
					%> <input type="checkbox"
					<%=genresIds.contains(genre.getId()) ? "checked" : ""%>
					name="genres[]" value="<%=genre.getId()%>"><%=genre.getName()%><br />
					<%
						}
					%>
				</td>
			</tr>
			<tr>
				<td class="label">&nbsp;</td>
				<td><input type="submit" value="Save Movie" /> <input
					type="hidden" name="id" value="<%=movie.getId()%>" /> <input
					type="hidden" name="action" value="save_edit" /></td>

			</tr>
		</table>
	</form>
	<a href="index.jsp">Go index page</a>
</body>
</html>