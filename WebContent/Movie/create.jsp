<%@page import="com.tolga.jss.model.Genre"%>
<%@page import="com.tolga.jss.model.Actor"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tolga.jss.model.Director"%>
<%@page import="java.util.List"%>
<%@page import="com.tolga.jss.model.Movie"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Movie movie = (Movie) request.getAttribute("movie");
	@SuppressWarnings("unchecked")
	List<Director> directors = (ArrayList<Director>) request.getAttribute("directors");
	@SuppressWarnings("unchecked")
	List<Actor> actors = (ArrayList<Actor>) request.getAttribute("actors");
	@SuppressWarnings("unchecked")
	List<Genre> genres = (ArrayList<Genre>) request.getAttribute("genres");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1>
		<%=request.getAttribute("page_text")%>
	</h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<form action="MovieServlet" method="post">
		<table>
			<tr>
				<td>Movie Title:</td>
				<td><input type="text" name="movie_title" /></td>
			</tr>
			<tr>
				<td>Movie Year:</td>
				<td><input type="text" name="movie_year" /></td>
			</tr>
			<tr>
				<td>Rating:</td>
				<td><input type="text" name="movie_rating" /></td>
			</tr>
			<tr>
				<td>Description:</td>
				<td><textarea name="movie_description"></textarea></td>
			</tr>
			<tr>
				<td>Director Name:</td>
				<td><select name="movie_director">
						<%
							for (Director director : directors) {
						%>
						<option value="<%=director.getId()%>"><%=director.getFirstName() + " " + director.getLastName()%></option>
						<%
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Actors:</td>
				<td>
					<%
						for (Actor actor : actors) {
					%> <input type="checkbox" name="actors[]"
					value="<%=actor.getId()%>"><%=actor.getFirstName() + " " + actor.getLastName()%><br />
					<%
						}
					%>
				</td>
			</tr>
			<tr>
				<td>Genre:</td>
				<td>
					<%
						for (Genre genre : genres) {
					%> <input type="checkbox" name="genres[]"
					value="<%=genre.getId()%>"><%=genre.getName()%><br /> <%
 	}
 %>
				</td>
			</tr>
			<tr>
				<td class="label">&nbsp;</td>
				<td><input type="submit" value="Save Movie" /> <input
					type="hidden" name="id" value="<%=movie.getId()%>" /> <input
					type="hidden" name="action" value="save_edit" /></td>

			</tr>
		</table>
	</form>
	<a href="index.jsp">Go index page</a>
</body>
</html>