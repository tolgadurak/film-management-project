<%@page import="com.tolga.jss.model.Genre"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.tolga.jss.model.Actor"%>
<%@page import="com.tolga.jss.model.Movie"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Movie movie = (Movie) request.getAttribute("movie");
	List<Actor> actors = (ArrayList<Actor>) movie.getActors();
	List<Genre> genres = (ArrayList<Genre>) movie.getGenres();
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<dl>
		<dt>Title:</dt>
		<dd><%=movie.getTitle()%></dd>
		<dt>Movie Year:</dt>
		<dd><%=movie.getYear()%></dd>
		<dt>Movie Rating:</dt>
		<dd><%=movie.getRating()%></dd>
		<dt>Director Name:</dt>
		<dd>
			<a
				href="DirectorServlet?action=details&id=<%=movie.getDirector_id()%>"><%=movie.getDirector_name()%></a>
		</dd>
		<dt>
			Actors in
			"<%=movie.getTitle()%>"</dt>
		<dd>
			<%
				for (int i = 0; i < actors.size(); i++) {
			%>
			<a href="ActorServlet?action=details&id=<%=actors.get(i).getId()%>"><%=actors.get(i).getFirstName() + " " + actors.get(i).getLastName()%></a>
			<%
				if (i < actors.size()-1)
						out.print(",");
			%>
			<%
				}
			%>
		</dd>
		<dt>
			Genre of 
			"<%=movie.getTitle()%>"</dt>
		<dd>
			<%
				for (int i = 0; i < genres.size(); i++) {
			%>
			<a href="GenreServlet?action=details&id=<%=genres.get(i).getId()%>"><%=genres.get(i).getName()%></a>
			<%
				if (i < genres.size()-1)
						out.print(",");
			%>
			<%
				}
			%>
		</dd>
		<dt></dt>
		<dd>
			<a href="MovieServlet?action=list">Back to list</a>
		</dd>
	</dl>
	<a href="index.jsp">Go index page</a>
</body>
</html>