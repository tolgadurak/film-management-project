<%@page import="com.tolga.jss.model.Movie"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	@SuppressWarnings("unchecked")
	List<Movie> movies = (ArrayList<Movie>) request.getAttribute("movies");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<div>
		<%
			if (username != null) {
		%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%
			}
		%>
	</div>
	<table>
		<tr>
			<th>Movie Title</th>
			<th>Year</th>
			<th>Rating</th>
			<th>Description</th>
			<th>Director Name</th>
		</tr>
		<%
			for (Movie movie : movies) {
		%>
		<tr>
			<td><%=movie.getTitle()%></td>
			<td><%=movie.getYear()%></td>
			<td><%=movie.getRating()%></td>
			<td><%=movie.getDescription()%></td>
			<td><a
				href="DirectorServlet?action=details&id=<%=movie.getDirector_id()%>"><%=movie.getDirector_name()%></a></td>
			<td><a href="MovieServlet?action=details&id=<%=movie.getId()%>">Details</a></td>
			<td><a href="MovieServlet?action=edit&id=<%=movie.getId()%>">Edit</a></td>
			<td><a href="MovieServlet?action=delete&id=<%=movie.getId()%>">Delete</a></td>
		</tr>
		<%
			}
		%>
	</table>
	<div>
		<a href="MovieServlet?action=create"
			title="Click here to insert new movie">Create New</a>
	</div>
	<a href="index.jsp">Go index page</a>
</body>
</html>