<%@page import="com.tolga.jss.model.Genre"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	Genre genre = (Genre) request.getAttribute("genre");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1>
		<%=request.getAttribute("page_text")%>
	</h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<form action="GenreServlet" method="post">
		<table>
			<tr>
				<td>Genre Name:</td>
				<td><input type="text" name="genre_name"
					value="<%=genre.getName()%>" /></td>
			</tr>

			<tr>
				<td class="label">&nbsp;</td>
				<td><input type="submit" value="Save Genre" /> <input
					type="hidden" name="id" value="<%=genre.getId()%>" /> <input
					type="hidden" name="action" value="save_edit" /></td>

			</tr>
		</table>
	</form>
	<a href="index.jsp">Go index page</a>
</body>
</html>