<%@page import="java.util.ArrayList"%>
<%@page import="com.tolga.jss.model.Genre"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	@SuppressWarnings("unchecked")
	List<Genre> genres = (ArrayList<Genre>) request.getAttribute("genres");
	String username = (String) session.getAttribute("username");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><%=request.getAttribute("page_text")%></title>
</head>
<body>
	<h1><%=request.getAttribute("page_text")%></h1>
	<div>
		<%if(username != null) {%>
		<span><%=username + " is logged in"%></span>
		<form action="AuthServlet" method="post">
			<input type="hidden" name="action" value="logout" /> <input
				type="submit" value="Logout" />
		</form>
		<%} %>
	</div>
	<table>
		<tr>
			<th>Genre Name</th>
		</tr>
		<%
			for (Genre genre : genres) {
		%>
		<tr>
			<td><%=genre.getName()%></td>
			<td><a href="GenreServlet?action=details&id=<%=genre.getId()%>">Details</a></td>
			<td><a href="GenreServlet?action=edit&id=<%=genre.getId()%>">Edit</a></td>
			<td><a href="GenreServlet?action=delete&id=<%=genre.getId()%>">Delete</a></td>
		</tr>
		<%
			}
		%>
	</table>
	<div>
		<a href="GenreServlet?action=create"
			title="Click here to insert new genre">Create New</a>
	</div>
	<a href="index.jsp">Go index page</a>
</body>
</html>