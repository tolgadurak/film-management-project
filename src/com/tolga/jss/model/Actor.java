package com.tolga.jss.model;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Actor {
	private int id;
	private String firstName;
	private String lastName;
	private boolean gender;
	private Date birthdate;
	private int rating;
	private String biography;

	public Actor() {

	}

	public Actor(int id, String firstName, String lastName, boolean gender, Date birthdate, int rating,
			String biography) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.birthdate = birthdate;
		this.rating = rating;
		this.biography = biography;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isGender() {
		return gender;
	}

	public String getGender() {
		return gender ? "Male" : "Female";
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public String getPresentationBirthDate() {
		return DateFormat.getDateInstance(DateFormat.SHORT, new Locale("tr", "TR")).format(getBirthdate());
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	/*
	 * public static void main(String[] args) { Actor a; try { SimpleDateFormat
	 * dateFormat = new SimpleDateFormat("dd.MM.yyyy"); Date date =
	 * dateFormat.parse("1.1.2014"); a = new Actor(1,"tolga", "durak", true,
	 * date,100,"sgfdsfgds"); Database db = new Database(); db.insertActor(a); }
	 * catch (ParseException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); }
	 * 
	 * }
	 */
}
