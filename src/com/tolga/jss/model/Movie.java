package com.tolga.jss.model;

import java.util.List;

public class Movie {
	private int id;
	private int director_id;
	private String director_name;
	private String title;
	private int year;
	private int rating;
	private String description;
	private List<Actor> actors;
	private List<Genre> genres;
	private List<Integer> actorIds;
	private List<Integer> genresIds;

	public Movie() {
		// TODO Auto-generated constructor stub
	}

	public Movie(int id, int director_id, String title, int year, int rating, String description) {
		super();
		this.id = id;
		this.director_id = director_id;
		this.title = title;
		this.year = year;
		this.rating = rating;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDirector_id() {
		return director_id;
	}

	public void setDirector_id(int director_id) {
		this.director_id = director_id;
	}

	public String getDirector_name() {
		return director_name;
	}

	public void setDirector_name(String director_name) {
		this.director_name = director_name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Actor> getActors() {
		return actors;
	}

	public void setActors(List<Actor> actors) {
		this.actors = actors;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public List<Integer> getActorIds() {
		return actorIds;
	}

	public void setActorIds(List<Integer> actorIds) {
		this.actorIds = actorIds;
	}

	public List<Integer> getGenresIds() {
		return genresIds;
	}

	public void setGenresIds(List<Integer> genresIds) {
		this.genresIds = genresIds;
	}

}
