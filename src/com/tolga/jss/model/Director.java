package com.tolga.jss.model;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class Director {
	private int id;
	private String firstName;
	private String lastName;
	private Date birthdate;
	private boolean gender;

	public Director() {
		// TODO Auto-generated constructor stub
	}

	public Director(int id, String firstName, String lastName, Date birthdate, boolean gender) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public String getPresentationBirthDate() {
		return DateFormat.getDateInstance(DateFormat.SHORT, new Locale("tr", "TR")).format(getBirthdate());
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public boolean isGender() {
		return gender;
	}

	public String getGender() {
		return gender ? "Male" : "Female";
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

}
