package com.obss.jss.login;


public class LoginServiceFactory {
	
	private static LoginServiceFactory factory = new LoginServiceFactory();
	private LoginService ls;
	
	public LoginServiceFactory(){
		ls = new LoginService();
	}
	
	public LoginService getLoginService(){
		return ls;
	}
	
	public static LoginServiceFactory getInstance(){
		return factory;
	}
}
