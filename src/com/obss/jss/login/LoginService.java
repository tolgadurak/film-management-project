package com.obss.jss.login;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.obss.jss.db.Database;
import com.tolga.jss.model.Admin;

public class LoginService {
	private Map<String, Admin> users;
	Database db = new Database();

	public LoginService() {
		users = new HashMap<String, Admin>();
		populateUsers(db.getAdmins());
	}

	public void populateUsers(List<Admin> admins) {
		for (Admin admin : admins) {
			users.put(admin.getName(), admin);
		}
	}

	public boolean authenticate(Admin loggingUser) {
		boolean isUser = false;
		String adminName = loggingUser.getName();
		if (users.containsKey(adminName)) {
			Admin user = users.get(adminName);
			if (user.getPassword().equals(loggingUser.getPassword()))
				isUser = true;
		}
		return isUser;
	}
}
