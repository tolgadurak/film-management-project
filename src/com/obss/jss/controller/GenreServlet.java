package com.obss.jss.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.obss.jss.db.Database;
import com.tolga.jss.model.Genre;

/**
 * Servlet implementation class GenreServlet
 */
@WebServlet("/GenreServlet")
public class GenreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PrintWriter out;
	private Database db;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GenreServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		db = new Database();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		out = response.getWriter();
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		String jsp = "";
		if (action == null)
			action = "list";
		db.connect();
		if (db.isConnected()) {
			if (action.equals("list")) {
				List<Genre> genres = new ArrayList<Genre>();
				genres = db.getGenres();
				request.setAttribute("genres", genres);
				request.setAttribute("page_text", "List Genres");
				jsp = "/Genre/list.jsp";
			} else if (action.equals("details")) {
				String idStr = request.getParameter("id");
				int genreId = 0;
				if (idStr != null) {
					try {
						genreId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						genreId = 0;
					}
				}
				Genre genre = db.getGenre(genreId);
				request.setAttribute("genre", genre);
				request.setAttribute("page_text", genre.getName() + "'s Details");
				jsp = "/Genre/details.jsp";
			} else if (action.equals("create")) {
				Genre genre = new Genre();
				genre.setId(-1);
				request.setAttribute("genre", genre);
				request.setAttribute("page_text", "Create New");
				jsp = "/Genre/create.jsp";
			} else if (action.equals("edit")) {
				String idStr = request.getParameter("id");
				int genreId = 0;
				if (idStr != null) {
					try {
						genreId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						genreId = 0;
					}
				}
				Genre genre = db.getGenre(genreId);
				request.setAttribute("genre", genre);
				request.setAttribute("page_text", "Edit " + genre.getName());
				jsp = "/Genre/edit.jsp";
			} else if (action.equals("delete")) {
				String idStr = request.getParameter("id");

				int genreId = 0;
				if (idStr != null) {
					try {
						genreId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						genreId = 0;
					}
					Genre genre = db.getGenre(genreId);
					request.setAttribute("genre", genre);
					jsp = "/Genre/delete.jsp";
				}
			}
			request.getRequestDispatcher(jsp).forward(request, response);
		} else {
			out.write("DB Connection Failed!");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		Genre genre = new Genre();
		if (!db.isConnected())
			db.connect();
		if (action != null) {
			if (action.equals("save_edit")) {
				String idStr = request.getParameter("id");
				String genreName = request.getParameter("genre_name");

				int genreId = 0;

				if (idStr != null) {
					try {
						genreId = Integer.parseInt(idStr);
						genre.setId(genreId);
					} catch (NumberFormatException e) {
						genreId = 0;
					}
				}
				genre.setName(genreName);

				if (genre.getId() > 0) {
					db.updateGenre(genre);
				} else {
					db.insertGenre(genre);
				}

			}else if (action.equals("delete_item")) {
				String idStr = request.getParameter("id");
				int genreId = 0;

				if (idStr != null) {
					try {
						genreId = Integer.parseInt(idStr);
						genre.setId(genreId);
					} catch (NumberFormatException e) {
						genreId = 0;
					}
				}
				db.deleteGenre(genreId);
			}
		}
		response.sendRedirect("GenreServlet?action=list");
	}

}
