package com.obss.jss.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.obss.jss.db.Database;
import com.tolga.jss.model.Director;

/**
 * Servlet implementation class DirectorServlet
 */
@WebServlet("/DirectorServlet")
public class DirectorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PrintWriter out;
	private Database db;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DirectorServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		db = new Database();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		out = response.getWriter();
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		String jsp = "";
		if (action == null)
			action = "list";
		db.connect();
		if (db.isConnected()) {
			if (action.equals("list")) {
				List<Director> directors = new ArrayList<Director>();
				directors = db.getDirectors();
				request.setAttribute("directors", directors);
				request.setAttribute("page_text", "List Directors");
				jsp = "/Director/list.jsp";
			} else if (action.equals("details")) {
				String idStr = request.getParameter("id");
				int directorId = 0;
				if (idStr != null) {
					try {
						directorId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						directorId = 0;
					}
				}
				Director director = db.getDirector(directorId);
				request.setAttribute("director", director);
				request.setAttribute("page_text", director.getFirstName() + "'s Details");
				jsp = "/Director/details.jsp";
			} else if (action.equals("create")) {
				Director director = new Director();
				director.setId(-1);
				request.setAttribute("director", director);
				request.setAttribute("page_text", "Create New");
				jsp = "/Director/create.jsp";
			} else if (action.equals("edit")) {
				String idStr = request.getParameter("id");
				int directorId = 0;
				if (idStr != null) {
					try {
						directorId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						directorId = 0;
					}
				}
				Director director = db.getDirector(directorId);
				request.setAttribute("director", director);
				request.setAttribute("page_text", "Edit " + director.getFirstName());
				jsp = "/Director/edit.jsp";
			} else if (action.equals("delete")) {
				String idStr = request.getParameter("id");

				int directorId = 0;
				if (idStr != null) {
					try {
						directorId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						directorId = 0;
					}
					Director director = db.getDirector(directorId);
					request.setAttribute("director", director);
					jsp = "/Director/delete.jsp";
				}
			}
			request.getRequestDispatcher(jsp).forward(request, response);
		} else {
			out.write("DB Connection Failed!");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("action");
		Director director = new Director();
		if (!db.isConnected())
			db.connect();
		if (action != null) {
			if (action.equals("save_edit")) {
				String idStr = request.getParameter("id");
				String firstName = request.getParameter("first_name");
				String lastName = request.getParameter("last_name");
				String gender = request.getParameter("gender");
				String birthdate = request.getParameter("birth_date");

				int directorId = 0;

				if (idStr != null) {
					try {
						directorId = Integer.parseInt(idStr);
						director.setId(directorId);
					} catch (NumberFormatException e) {
						directorId = 0;
					}
				}
				director.setFirstName(firstName);
				director.setLastName(lastName);
				director.setGender(gender.equalsIgnoreCase("M") ? true : false);
				SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
				try {
					director.setBirthdate(sdf.parse(birthdate));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (director.getId() > 0) {
					db.updateDirector(director);
				} else {
					db.insertDirector(director);
				}

			} else if (action.equals("delete_item")) {
				String idStr = request.getParameter("id");
				int directorId = 0;

				if (idStr != null) {
					try {
						directorId = Integer.parseInt(idStr);
						director.setId(directorId);
					} catch (NumberFormatException e) {
						directorId = 0;
					}
				}
				db.deleteDirector(directorId);
			}
		}
		response.sendRedirect("DirectorServlet?action=list");
	}

}
