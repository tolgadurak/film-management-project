package com.obss.jss.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.obss.jss.db.Database;
import com.obss.jss.login.LoginService;
import com.obss.jss.login.LoginServiceFactory;
import com.tolga.jss.model.Admin;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/AuthServlet")
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private Database db;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		db = new Database();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = null;
		Cookie cookie = null;
		String action = request.getParameter("action");
		if (action != null) {
			if (action.equals("login")) {
				String username = request.getParameter("user_name");
				String password = request.getParameter("user_password");
				Admin admin = new Admin();
				admin.setName(username);
				admin.setPassword(password);
				LoginServiceFactory loginServiceFactory = LoginServiceFactory.getInstance();
				LoginService loginService = loginServiceFactory.getLoginService();
				boolean outcome = loginService.authenticate(admin);
				if (outcome) {
					session = request.getSession(true);
					session.setAttribute("username", username);
					cookie = new Cookie("sessionId", session.getId());
					response.addCookie(cookie);
					if (session.getAttribute("attempt") != null)
						response.sendRedirect(session.getAttribute("attempt").toString());
					else
						response.sendRedirect("index.jsp");
				}
				else {
					response.getWriter().write("Username or password is not correct. Please <a href=\"login.jsp\">try again</a>");
				}
			} else if (action.equals("logout")) {
				session = request.getSession();
				session.removeAttribute("username");
				session.invalidate();
				response.sendRedirect("index.jsp");
			}
		}

	}

}
