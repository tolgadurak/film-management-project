package com.obss.jss.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.obss.jss.db.Database;
import com.tolga.jss.model.Actor;
import com.tolga.jss.model.Director;
import com.tolga.jss.model.Genre;
import com.tolga.jss.model.Movie;

/**
 * Servlet implementation class MovieServlet
 */
@WebServlet("/MovieServlet")
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PrintWriter out;
	private Database db;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MovieServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		db = new Database();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		out = response.getWriter();
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		String jsp = "";
		if (action == null)
			action = "list";
		db.connect();
		if (db.isConnected()) {
			if (action.equals("list")) {
				List<Movie> movies = new ArrayList<Movie>();
				movies = db.getMovies();
				request.setAttribute("movies", movies);
				request.setAttribute("page_text", "List Movies");
				jsp = "/Movie/list.jsp";
			} else if (action.equals("details")) {
				String idStr = request.getParameter("id");
				int movieId = 0;
				if (idStr != null) {
					try {
						movieId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						movieId = 0;
					}
				}
				Movie movie = db.getMovie(movieId);
				movie.setActors(db.getActorsInMovie(movieId));
				movie.setGenres(db.getGenresOfMovie(movieId));
				request.setAttribute("movie", movie);
				request.setAttribute("page_text", movie.getTitle() + "'s Details");
				jsp = "/Movie/details.jsp";
			} else if (action.equals("create")) {
				Movie movie = new Movie();
				List<Director> directors = db.getDirectors();
				List<Actor> actors = db.getActors();
				List<Genre> genres = db.getGenres();
				movie.setId(-1);
				request.setAttribute("movie", movie);
				request.setAttribute("directors", directors);
				request.setAttribute("actors", actors);
				request.setAttribute("genres", genres);
				request.setAttribute("page_text", "Create New");
				jsp = "/Movie/create.jsp";
			} else if (action.equals("edit")) {

				String idStr = request.getParameter("id");
				int movieId = 0;
				if (idStr != null) {
					try {
						movieId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						movieId = 0;
					}
				}
				List<Director> directors = db.getDirectors();
				List<Actor> actors = db.getActors();
				List<Genre> genres = db.getGenres();
				Movie movie = db.getMovie(movieId);
				movie.setActorIds(db.getActorsIdsInMovie(movieId));
				movie.setGenresIds(db.getGenresIdsOfMovie(movieId));
				request.setAttribute("movie", movie);
				request.setAttribute("directors", directors);
				request.setAttribute("actors", actors);
				request.setAttribute("genres", genres);
				request.setAttribute("page_text", "Edit " + movie.getTitle());
				jsp = "/Movie/edit.jsp";
			} else if (action.equals("delete")) {
				String idStr = request.getParameter("id");

				int movieId = 0;
				if (idStr != null) {
					try {
						movieId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						movieId = 0;
					}
					Movie movie = db.getMovie(movieId);
					movie.setActors(db.getActorsInMovie(movieId));
					movie.setGenres(db.getGenresOfMovie(movieId));
					request.setAttribute("movie", movie);
					jsp = "/Movie/delete.jsp";
				}
			}
			request.getRequestDispatcher(jsp).forward(request, response);
		} else {
			out.write("DB Connection Failed!");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		Movie movie = new Movie();
		if (!db.isConnected())
			db.connect();
		if (action != null) {
			if (action.equals("save_edit")) {
				String idStr = request.getParameter("id");
				String title = request.getParameter("movie_title");
				String year = request.getParameter("movie_year");
				String rating = request.getParameter("movie_rating");
				String description = request.getParameter("movie_description");
				String directorId = request.getParameter("movie_director");
				String[] actorIds = request.getParameterValues("actors[]");
				String[] genreIds = request.getParameterValues("genres[]");
				int movieId = 0;

				if (idStr != null) {
					try {
						movieId = Integer.parseInt(idStr);
						movie.setId(movieId);
					} catch (NumberFormatException e) {
						movieId = 0;
					}
				}
				movie.setTitle(title);
				int movie_year = 0;
				if (year != null) {
					try {
						movie_year = Integer.parseInt(year);
					} catch (NumberFormatException e) {
					}
				}
				movie.setYear(movie_year);
				int movie_rating = 0;
				if (rating != null) {
					try {
						movie_rating = Integer.parseInt(rating);
					} catch (NumberFormatException e) {
					}
				}
				movie.setRating(movie_rating);
				movie.setDescription(description);
				int movie_director_id = 0;
				if (directorId != null) {
					try {
						movie_director_id = Integer.parseInt(directorId);
					} catch (NumberFormatException e) {
					}
				}
				movie.setDirector_id(movie_director_id);
				if (movie.getId() > 0) {
					db.deleteRelationsOfMovie(movieId);
					db.updateMovie(movie);// Not completed
					if (actorIds != null) {
						for (String id : actorIds) {
							int actorId = 0;
							if (id != null) {
								try {
									actorId = Integer.parseInt(id);
								} catch (NumberFormatException e) {
								}
							}
							db.insertMovieActor(movieId, actorId);
						}
					}
					if (genreIds != null) {
						for (String id : genreIds) {
							int genreId = 0;
							if (id != null) {
								try {
									genreId = Integer.parseInt(id);
								} catch (NumberFormatException e) {
								}
							}
							db.insertMovieGenre(movieId, genreId);
						}
					}
				} else {
					movieId = db.insertMovie(movie);
					if (actorIds != null) {
						for (String id : actorIds) {
							int actorId = 0;
							if (id != null) {
								try {
									actorId = Integer.parseInt(id);
								} catch (NumberFormatException e) {
								}
							}
							db.insertMovieActor(movieId, actorId);
						}
					}
					if (genreIds != null) {
						for (String id : genreIds) {
							int genreId = 0;
							if (id != null) {
								try {
									genreId = Integer.parseInt(id);
								} catch (NumberFormatException e) {
								}
							}
							db.insertMovieGenre(movieId, genreId);
						}
					}
				}

			} else if (action.equals("delete_item")) {
				String idStr = request.getParameter("id");
				int movieId = 0;

				if (idStr != null) {
					try {
						movieId = Integer.parseInt(idStr);
						movie.setId(movieId);
					} catch (NumberFormatException e) {
						movieId = 0;
					}
				}
				db.deleteMovie(movieId);
			}
		}
		response.sendRedirect("MovieServlet?action=list");
	}

}
