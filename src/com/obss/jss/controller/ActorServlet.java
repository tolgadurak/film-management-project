package com.obss.jss.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.obss.jss.db.Database;
import com.tolga.jss.model.Actor;

/**
 * Servlet implementation class ActorServlet
 */
@WebServlet("/ActorServlet")
public class ActorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PrintWriter out;
	private Database db;

	public ActorServlet() {
		super();
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		db = new Database();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		out = response.getWriter();
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");
		String jsp = "";
		if (action == null)
			action = "list";
		db.connect();
		if (db.isConnected()) {
			if (action.equals("list")) {
				List<Actor> actors = new ArrayList<Actor>();
				actors = db.getActors();
				request.setAttribute("actors", actors);
				request.setAttribute("page_text", "List Actors");
				jsp = "/Actor/list.jsp";
			} else if (action.equals("details")) {
				String idStr = request.getParameter("id");
				int actorId = 0;
				if (idStr != null) {
					try {
						actorId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						actorId = 0;
					}
				}
				Actor actor = db.getActor(actorId);
				request.setAttribute("actor", actor);
				request.setAttribute("page_text", actor.getFirstName() + "'s Details");
				jsp = "/Actor/details.jsp";
			} else if (action.equals("create")) {
				Actor actor = new Actor();
				actor.setId(-1);
				request.setAttribute("actor", actor);
				request.setAttribute("page_text", "Create New");
				jsp = "/Actor/create.jsp";
			} else if (action.equals("edit")) {
				String idStr = request.getParameter("id");
				int actorId = 0;
				if (idStr != null) {
					try {
						actorId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						actorId = 0;
					}
				}
				Actor actor = db.getActor(actorId);
				request.setAttribute("actor", actor);
				request.setAttribute("page_text", "Edit " + actor.getFirstName());
				jsp = "/Actor/edit.jsp";
			} else if (action.equals("delete")) {
				String idStr = request.getParameter("id");

				int actorId = 0;
				if (idStr != null) {
					try {
						actorId = Integer.parseInt(idStr);
					} catch (NumberFormatException e) {
						actorId = 0;
					}
					Actor actor = db.getActor(actorId);
					request.setAttribute("actor", actor);
					jsp = "/Actor/delete.jsp";
				}
			}
			request.getRequestDispatcher(jsp).forward(request, response);
		} else {
			out.write("DB Connection Failed!");
		}

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		Actor actor = new Actor();
		if (!db.isConnected())
			db.connect();
		if (action != null) {
			if (action.equals("save_edit")) {
				String idStr = request.getParameter("id");
				String firstName = request.getParameter("first_name");
				String lastName = request.getParameter("last_name");
				String gender = request.getParameter("gender");
				String birthdate = request.getParameter("birth_date");
				String rating = request.getParameter("rating");
				String biography = request.getParameter("biography");
				int actorId = 0;

				if (idStr != null) {
					try {
						actorId = Integer.parseInt(idStr);
						actor.setId(actorId);
					} catch (NumberFormatException e) {
						actorId = 0;
					}
				}
				actor.setFirstName(firstName);
				actor.setLastName(lastName);
				actor.setGender(gender.equalsIgnoreCase("M") ? true : false);
				SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
				try {
					actor.setBirthdate(sdf.parse(birthdate));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					int intRating = 0;
					intRating = Integer.parseInt(rating);
					actor.setRating(intRating);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				actor.setBiography(biography);
				if (actor.getId() > 0) {
					db.updateActor(actor);
				} else {
					db.insertActor(actor);
				}

			}
			else if(action.equals("delete_item")) {
				String idStr = request.getParameter("id");
				int actorId = 0;

				if (idStr != null) {
					try {
						actorId = Integer.parseInt(idStr);
						actor.setId(actorId);
					} catch (NumberFormatException e) {
						actorId = 0;
					}
				}
				db.deleteActor(actorId);
			}
		}
		response.sendRedirect("ActorServlet?action=list");
	}

}
