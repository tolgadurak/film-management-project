package com.obss.jss.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class FilmManagementFilter
 */
@WebFilter("/*")
public class FilmManagementFilter implements Filter {
	String str = "";

	/**
	 * Default constructor.
	 */
	public FilmManagementFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// request.getRequestDispatcher("login.jsp").forward(request, response);
		HttpSession session = null;
		HttpServletRequest req = null;
		HttpServletResponse resp = null;
		String username = null;
		String requestedURI = null;
		String queryString = null;
		req = (HttpServletRequest) request;
		resp = (HttpServletResponse) response;
		requestedURI = req.getRequestURI();
		queryString = req.getQueryString();

		session = req.getSession();
		if (session != null) {
			username = (String) session.getAttribute("username");
			if (username == null) {
				if (requestedURI.contains("/ActorServlet") || requestedURI.contains("/DirectorServlet")
						|| requestedURI.contains("/GenreServlet") || requestedURI.contains("/MovieServlet")) {
					session.setAttribute("attempt", requestedURI + "?" + queryString);
					resp.sendRedirect("login.jsp");
				} else {
					chain.doFilter(request, response);
				}
			} else {
				if (requestedURI.contains("/login.jsp")) {
					resp.sendRedirect("index.jsp");
				} else {
					chain.doFilter(request, response);
				}
			}
		}
		// chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
