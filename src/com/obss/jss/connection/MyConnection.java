package com.obss.jss.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {
	private static final String host = "localhost";
	private static final String port = "3306";
	private static final String user = "root";
	private static final String pass = "";
	protected static final String dbName = "film_Management"; //Automatically creates a databases with that name if not exists.
	protected String connString;
	protected boolean isConnected;
	protected Connection conn;

	public void connect() {
		connString = "jdbc:mysql://" + host + ":" + port + "/"+dbName;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(connString, user, pass);
			isConnected = true;
		} catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void disconnect() {
		try {
			conn.close();
			isConnected = false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public static String getHost() {
		return host;
	}

	public static String getUser() {
		return user;
	}

	public static String getPass() {
		return pass;
	}

	public static String getDbname() {
		return dbName;
	}
}
