package com.obss.jss.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.obss.jss.connection.MyConnection;

public class DatabaseInit extends MyConnection {
	private PreparedStatement ps;
	private String CREATE_DB = "CREATE DATABASE IF NOT EXISTS ?";
	private String CREATE_ACTOR = "CREATE TABLE IF NOT EXISTS `actor`"
			+ " (`id` int(11) NOT NULL AUTO_INCREMENT,`first_name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,"
			+ "`last_name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,`gender` tinyint(1) NOT NULL,"
			+ "`birthdate` datetime NOT NULL,`rating` float NOT NULL,`biography` text COLLATE utf8_turkish_ci,"
			+ "PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=1 ;";
	private String CREATE_DIRECTOR = "CREATE TABLE IF NOT EXISTS `director`"
			+ " (`id` int(11) NOT NULL AUTO_INCREMENT,`first_name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,"
			+ "`last_name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,`gender` tinyint(1) NOT NULL,"
			+ "`birthdate` datetime NOT NULL,PRIMARY KEY (`id`),KEY `id` (`id`)) "
			+ "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=1 ;";
	private String CREATE_GENRE = "CREATE TABLE IF NOT EXISTS `genre` "
			+ "(`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,"
			+ "PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=1 ;";
	private String CREATE_MOVIE = "CREATE TABLE IF NOT EXISTS `movie` (`id` int(11) NOT NULL AUTO_INCREMENT,"
			+ "`title` varchar(100) COLLATE utf8_turkish_ci NOT NULL,`movie_year` year(4) NOT NULL,`rating` float NOT NULL,"
			+ "`description` text COLLATE utf8_turkish_ci NOT NULL,`director_id` int(11) NOT NULL,PRIMARY KEY (`id`),"
			+ "KEY `director_id` (`director_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci "
			+ "AUTO_INCREMENT=1 ;";
	private String CREATE_MOVIE_ACTOR_MAP = "CREATE TABLE IF NOT EXISTS `movie_actor_map` "
			+ "(`movie_id` int(11) NOT NULL,`actor_id` int(11) NOT NULL,KEY `movie_id` (`movie_id`,`actor_id`),"
			+ "KEY `actor_id` (`actor_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;";
	private String CREATE_MOVIE_GENRE_MAP = "CREATE TABLE IF NOT EXISTS `movie_genre_map` "
			+ "(`movie_id` int(11) NOT NULL,`genre_id` int(11) NOT NULL,KEY `movie_id` (`movie_id`,`genre_id`),"
			+ "KEY `genre_id` (`genre_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;";
	private String ALTER_TABLE_MOVIE = "ALTER TABLE `movie` ADD CONSTRAINT `movie_ibfk_1` "
			+ "FOREIGN KEY (`director_id`) REFERENCES `director` (`id`);";
	private String ALTER_TABLE_MAP1 = "ALTER TABLE `movie_actor_map` ADD CONSTRAINT "
			+ "`movie_actor_map_ibfk_2` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`id`), "
			+ "ADD CONSTRAINT `movie_actor_map_ibfk_1` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`);";
	private String ALTER_TABLE_MAP2 = "ALTER TABLE `movie_genre_map` ADD CONSTRAINT `movie_genre_map_ibfk_2` "
			+ "FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`), ADD CONSTRAINT `movie_genre_map_ibfk_1` "
			+ "FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`);";
	private String CREATE_ADMIN = "CREATE TABLE IF NOT EXISTS `admin` (`id` int(11) NOT NULL AUTO_INCREMENT,"
			+ "`name` varchar(100) COLLATE utf8_turkish_ci NOT NULL,`password` varchar(20) "
			+ "COLLATE utf8_turkish_ci NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB "
			+ "DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci AUTO_INCREMENT=1 ;";

	public DatabaseInit() {
		CREATE_DB = CREATE_DB.replace("?", dbName);
	}

	public void initDatabase() {

		List<String> commands = new ArrayList<String>();
		DatabaseRecords records = new DatabaseRecords();
		commands.add(CREATE_ADMIN);
		commands.add(CREATE_ACTOR);
		commands.add(CREATE_DIRECTOR);
		commands.add(CREATE_GENRE);
		commands.add(CREATE_MOVIE);
		commands.add(CREATE_MOVIE_ACTOR_MAP);
		commands.add(CREATE_MOVIE_GENRE_MAP);
		commands.add(ALTER_TABLE_MOVIE);
		commands.add(ALTER_TABLE_MAP1);
		commands.add(ALTER_TABLE_MAP2);
		commands.add(records.getINSERT_ADMIN());
		commands.add(records.getINSERT_ACTORS());
		commands.add(records.getINSERT_DIRECTORS());
		commands.add(records.getINSERT_GENRES());
		commands.add(records.getINSERT_MOVIES());
		commands.add(records.getINSERT_MOVIE_ACTOR());
		commands.add(records.getINSERT_MOVIE_GENRE());
		
		if (!createDB()) {
			try {

				if (!isConnected)
					connect();
				for (String cmd : commands) {
					setPs(conn.prepareStatement(cmd));
					ps.execute();
				}

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				disconnect();
			}
		}
	}

	private boolean createDB() {
		Connection cn = null;
		boolean isDbExists = false;
		try {
			cn = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "");
			PreparedStatement ps = cn.prepareStatement("SHOW DATABASES LIKE ?;");
			ps.setString(1, dbName);
			ResultSet rs = ps.executeQuery();
			isDbExists = rs.next();
			cn.prepareStatement(CREATE_DB).executeUpdate();
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isDbExists;
	}

	public PreparedStatement getPs() {
		return ps;
	}

	public void setPs(PreparedStatement ps) {
		this.ps = ps;
	}
}
