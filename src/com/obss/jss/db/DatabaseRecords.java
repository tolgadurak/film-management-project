package com.obss.jss.db;

public class DatabaseRecords {
	private String INSERT_ACTORS = "INSERT INTO `actor` (`id`, `first_name`, `last_name`, `gender`, `birthdate`, `rating`, `biography`) VALUES(6, 'Bruce', 'Willis', 1, '1955-03-19 00:00:00', 100, 'Actor and musician Bruce Willis is well known for playing wisecracking or hard-edged characters, often in spectacular action films.'),(7, 'Amanda', 'Amanda Seyfried', 0, '1985-12-03 00:00:00', 85, 'Amanda Seyfried was born and raised in Allentown, Pennsylvania, to Ann (Sander), an occupational therapist, and Jack Seyfried, a pharmacist.'),(8, 'Johnny', 'Depp', 1, '1963-07-09 00:00:00', 100, 'Johnny Depp is perhaps one of the most versatile actors of his day and age in Hollywood. '),(9, 'Jennifer', 'Lawrence', 0, '1990-08-15 00:00:00', 85, 'Academy Award-winning actress Jennifer Lawrence is best known for playing Katniss Everdeen in The Hunger Games (2012)'),(10, 'William', 'Atherton', 1, '1947-07-30 00:00:00', 75, 'William Atherton Knight was born on July 30, 1947 in Orange, Connecticut, the son of Robert and Roby Atherton Knight.'),(11, 'Hart', 'Bochner', 1, '1956-10-03 00:00:00', 70, 'Bochner made his feature film debut portraying George C. Scott''s son in Ernest Hemingway''s Islands in the Stream'),(12, 'Justin', 'Timberlake', 1, '1981-01-31 00:00:00', 90, 'Justin Randall Timberlake was born on January 31, 1981, in Memphis, Tennessee.'),(13, 'Cillian', 'Murphy', 1, '1976-05-25 00:00:00', 100, 'Striking Irish actor Cillian Murphy was born in Douglas, the oldest child of Brendan Murphy, who works for the Irish Department of Education, and a mother who is a teacher of French.'),(14, 'Wes', 'Bentley', 1, '1978-09-04 00:00:00', 60, 'Wes Bentley is an American actor who first became well-known via his role in the Oscar-winning film American Beauty (1999)'),(15, 'Paula', 'Malcomson', 1, '1970-07-28 00:00:00', 70, 'Born in 1970 in Belfast, Northern Ireland');";
	private String INSERT_DIRECTORS = "INSERT INTO `director` (`id`, `first_name`, `last_name`, `gender`, `birthdate`) VALUES(4, 'John', 'McTiernan', 1, '1951-01-08 00:00:00'),(5, 'Andrew', 'Niccol', 1, '1964-06-10 00:00:00'),(6, 'Gary', 'Ross', 1, '1956-11-03 00:00:00');";
	private String INSERT_GENRES = "INSERT INTO `genre` (`id`, `name`) VALUES(4, 'Adventure'),(5, 'Sci-Fi'),(6, 'Thriller'),(7, 'Action');";
	private String INSERT_MOVIES = "INSERT INTO `movie` (`id`, `title`, `movie_year`, `rating`, `description`, `director_id`) VALUES(12, 'Die Hard', 1988, 100, 'John McClane, officer of the NYPD, tries to save wife Holly Gennaro and several others, taken hostage by German terrorist Hans Gruber during a Christmas party at the Nakatomi Plaza in Los Angeles.', 4),(13, 'In Time', 2011, 100, 'In a future where people stop aging at 25, but are engineered to live only one more year, having the means to buy your way out of the situation is a shot at immortal youth.', 5),(14, 'The Hunger Games', 2012, 85, 'Katniss Everdeen voluntarily takes her younger sister''s place in the Hunger Games, a televised fight to the death in which two teenagers from each of the twelve Districts of Panem are chosen at random to compete.', 6);";
	private String INSERT_MOVIE_ACTOR = "INSERT INTO `movie_actor_map` (`movie_id`, `actor_id`) VALUES(12, 6),(12, 10),(12, 11),(13, 7),(13, 12),(13, 13);";
	private String INSERT_MOVIE_GENRE = "INSERT INTO `movie_genre_map` (`movie_id`, `genre_id`) VALUES(12, 6),(12, 7),(13, 5),(13, 6),(13, 7),(14, 4),(14, 5),(14, 6);";
	private String INSERT_ADMIN = "INSERT INTO `admin` (`id`, `name`, `password`) VALUES(2, 'admin', 'admin');";
	public String getINSERT_ACTORS() {
		return INSERT_ACTORS;
	}
	public void setINSERT_ACTORS(String iNSERT_ACTORS) {
		INSERT_ACTORS = iNSERT_ACTORS;
	}
	public String getINSERT_DIRECTORS() {
		return INSERT_DIRECTORS;
	}
	public void setINSERT_DIRECTORS(String iNSERT_DIRECTORS) {
		INSERT_DIRECTORS = iNSERT_DIRECTORS;
	}
	public String getINSERT_GENRES() {
		return INSERT_GENRES;
	}
	public void setINSERT_GENRES(String iNSERT_GENRES) {
		INSERT_GENRES = iNSERT_GENRES;
	}
	public String getINSERT_MOVIES() {
		return INSERT_MOVIES;
	}
	public void setINSERT_MOVIES(String iNSERT_MOVIES) {
		INSERT_MOVIES = iNSERT_MOVIES;
	}
	public String getINSERT_MOVIE_ACTOR() {
		return INSERT_MOVIE_ACTOR;
	}
	public void setINSERT_MOVIE_ACTOR(String iNSERT_MOVIE_ACTOR) {
		INSERT_MOVIE_ACTOR = iNSERT_MOVIE_ACTOR;
	}
	public String getINSERT_MOVIE_GENRE() {
		return INSERT_MOVIE_GENRE;
	}
	public void setINSERT_MOVIE_GENRE(String iNSERT_MOVIE_GENRE) {
		INSERT_MOVIE_GENRE = iNSERT_MOVIE_GENRE;
	}
	public String getINSERT_ADMIN() {
		return INSERT_ADMIN;
	}
	public void setINSERT_ADMIN(String iNSERT_ADMIN) {
		INSERT_ADMIN = iNSERT_ADMIN;
	}
	
	
}
