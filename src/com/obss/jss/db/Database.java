package com.obss.jss.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.obss.jss.connection.MyConnection;
import com.tolga.jss.model.Actor;
import com.tolga.jss.model.Admin;
import com.tolga.jss.model.Director;
import com.tolga.jss.model.Genre;
import com.tolga.jss.model.Movie;

public class Database extends MyConnection {

	private List<String> tableNames;
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private DatabaseInit dbInit;

	public Database() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbInit = new DatabaseInit();
		dbInit.initDatabase();
	}

	@SuppressWarnings("unused")
	private List<String> getTableNames() {

		tableNames = new ArrayList<String>();
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("SHOW TABLES");
			rs = ps.executeQuery();
			while (rs.next()) {
				tableNames.add(rs.getString(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			disconnect();
		}
		return tableNames;
	}

	public List<Actor> getActors() {

		ArrayList<Actor> list = new ArrayList<Actor>();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM ACTOR");
			rs = ps.executeQuery();
			while (rs.next()) {
				Actor actor = new Actor();
				actor.setId(rs.getInt("id"));
				actor.setFirstName(rs.getString("first_name"));
				actor.setLastName(rs.getString("last_name"));
				actor.setGender(rs.getBoolean("gender"));
				actor.setBirthdate(rs.getDate("birthdate"));
				actor.setRating(rs.getInt("rating"));
				actor.setBiography(rs.getString("biography"));
				list.add(actor);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Admin> getAdmins() {
		ArrayList<Admin> list = new ArrayList<Admin>();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM ADMIN");
			rs = ps.executeQuery();
			while (rs.next()) {
				Admin admin = new Admin();
				admin.setId(rs.getInt("id"));
				admin.setName(rs.getString("name"));
				admin.setPassword(rs.getString("password"));
				list.add(admin);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}
	}

	public List<Director> getDirectors() {

		ArrayList<Director> list = new ArrayList<Director>();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM DIRECTOR");
			rs = ps.executeQuery();
			while (rs.next()) {
				Director director = new Director();
				director.setId(rs.getInt("id"));
				director.setFirstName(rs.getString("first_name"));
				director.setLastName(rs.getString("last_name"));
				director.setGender(rs.getBoolean("gender"));
				director.setBirthdate(rs.getDate("birthdate"));
				list.add(director);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Genre> getGenres() {

		ArrayList<Genre> list = new ArrayList<Genre>();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM GENRE");
			rs = ps.executeQuery();
			while (rs.next()) {
				Genre genre = new Genre();
				genre.setId(rs.getInt("id"));
				genre.setName(rs.getString("name"));
				list.add(genre);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Movie> getMovies() {

		ArrayList<Movie> list = new ArrayList<Movie>();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM MOVIE m INNER JOIN DIRECTOR d on m.director_id = d.id ");
			rs = ps.executeQuery();
			while (rs.next()) {
				Movie movie = new Movie();
				movie.setId(rs.getInt("m.id"));
				movie.setDirector_id(rs.getInt("m.director_id"));
				movie.setDirector_name(rs.getString("d.first_name") + " " + rs.getString("d.last_name"));
				movie.setTitle(rs.getString("m.title"));
				movie.setYear(rs.getInt("m.movie_year"));
				movie.setDescription(rs.getString("m.description"));
				movie.setRating(rs.getInt("m.rating"));
				list.add(movie);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Actor> getActorsInMovie(int movieId) {

		ArrayList<Actor> actors = new ArrayList<Actor>();
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("SELECT * FROM MOVIE_ACTOR_MAP map INNER JOIN "
					+ "ACTOR a ON map.actor_id = a.id WHERE map.movie_id = ? ");
			ps.setInt(1, movieId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Actor actor = new Actor();
				actor.setId(rs.getInt("actor_id"));
				actor.setFirstName(rs.getString("first_name"));
				actor.setLastName(rs.getString("last_name"));
				actor.setGender(rs.getBoolean("gender"));
				actor.setBirthdate(rs.getDate("birthdate"));
				actor.setBiography(rs.getString("biography"));
				actor.setRating(rs.getInt("rating"));
				actors.add(actor);
			}
			return actors;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Integer> getActorsIdsInMovie(int movieId) {

		ArrayList<Integer> actorsIds = new ArrayList<Integer>();
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("SELECT * FROM MOVIE_ACTOR_MAP map INNER JOIN "
					+ "ACTOR a ON map.actor_id = a.id WHERE map.movie_id = ? ");
			ps.setInt(1, movieId);
			rs = ps.executeQuery();
			while (rs.next()) {
				actorsIds.add(rs.getInt("actor_id"));
			}
			return actorsIds;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Movie> getMoviesOfActor(int actorId) {

		ArrayList<Movie> movies = new ArrayList<Movie>();
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("SELECT * FROM MOVIE_ACTOR_MAP map INNER JOIN "
					+ "MOVIE m on map.movie_id = m.id WHERE map.actorId = ?");
			ps.setInt(1, actorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Movie movie = new Movie();
				movie.setId(rs.getInt("movie_id"));
				movie.setTitle(rs.getString("title"));
				movie.setDescription(rs.getString("description"));
				movie.setDirector_id(rs.getInt("director_id"));
				movie.setRating(rs.getInt("rating"));
				movie.setYear(rs.getInt("movie_year"));
				movies.add(movie);
			}
			return movies;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Movie> getMoviesOfGenre(int genreId) {

		ArrayList<Movie> movies = new ArrayList<Movie>();
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("SELECT * FROM MOVIE_GENRE_MAP map "
					+ "INNER JOIN MOVIE m ON map.movie_id = m.id WHERE map.genre_id = ?");
			ps.setInt(1, genreId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Movie movie = new Movie();
				movie.setId(rs.getInt("movie_id"));
				movie.setTitle(rs.getString("title"));
				movie.setDescription(rs.getString("description"));
				movie.setDirector_id(rs.getInt("director_id"));
				movie.setRating(rs.getInt("rating"));
				movie.setYear(rs.getInt("movie_year"));
				movies.add(movie);
			}
			return movies;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Genre> getGenresOfMovie(int movieId) {

		ArrayList<Genre> genres = new ArrayList<Genre>();
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("SELECT * FROM MOVIE_GENRE_MAP map INNER JOIN "
					+ "GENRE g ON map.genre_id = g.id WHERE map.movie_id = ?");
			ps.setInt(1, movieId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Genre genre = new Genre();
				genre.setId(rs.getInt("id"));
				genre.setName(rs.getString("name"));
				genres.add(genre);
			}
			return genres;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public List<Integer> getGenresIdsOfMovie(int movieId) {

		ArrayList<Integer> genresIds = new ArrayList<Integer>();
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("SELECT * FROM MOVIE_GENRE_MAP map INNER JOIN "
					+ "GENRE g ON map.genre_id = g.id WHERE map.movie_id = ?");
			ps.setInt(1, movieId);
			rs = ps.executeQuery();
			while (rs.next()) {
				genresIds.add(rs.getInt("g.id"));
			}
			return genresIds;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}

	}

	public void insertActor(Actor actor) {

		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement(
					"INSERT INTO ACTOR (first_name, last_name, gender, birthdate, rating, biography) VALUES(?,?,?,?,?,?)");
			ps.setString(1, actor.getFirstName());
			ps.setString(2, actor.getLastName());
			ps.setBoolean(3, actor.isGender());
			ps.setDate(4, new java.sql.Date(actor.getBirthdate().getTime()));
			ps.setInt(5, actor.getRating());
			ps.setString(6, actor.getBiography());
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void insertAdmin(Admin admin) {
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("INSERT INTO ADMIN (name,password) VALUES(?,?)");
			ps.setString(1, admin.getName());
			ps.setString(2, admin.getPassword());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void insertDirector(Director director) {

		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement(
					"INSERT INTO DIRECTOR (first_name, last_name, gender, birthdate) VALUES(?,?,?,?)");
			ps.setString(1, director.getFirstName());
			ps.setString(2, director.getLastName());
			ps.setBoolean(3, director.isGender());
			ps.setDate(4, new java.sql.Date(director.getBirthdate().getTime()));
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void insertGenre(Genre genre) {

		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("INSERT INTO GENRE (name) VALUES(?)");
			ps.setString(1, genre.getName());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}

	}

	public int insertMovie(Movie movie) {
		int lastInsertedId = 0;
		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement(
					"INSERT INTO MOVIE (title, movie_year, rating, description, director_id) VALUES(?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, movie.getTitle());
			ps.setInt(2, movie.getYear());
			ps.setInt(3, movie.getRating());
			ps.setString(4, movie.getDescription());
			ps.setInt(5, movie.getDirector_id());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				lastInsertedId = rs.getInt(1);
			}
			return lastInsertedId;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		} finally {
			disconnect();
		}

	}

	public void insertMovieActor(int movieId, int actorId) {

		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("INSERT INTO MOVIE_ACTOR_MAP (movie_id, actor_id) VALUES(?,?)");
			ps.setInt(1, movieId);
			ps.setInt(2, actorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void insertMovieGenre(int movieId, int genreId) {

		if (!isConnected)
			connect();
		try {
			ps = conn.prepareStatement("INSERT INTO MOVIE_GENRE_MAP (movie_id, genre_id) VALUES(?,?)");
			ps.setInt(1, movieId);
			ps.setInt(2, genreId);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void updateActor(Actor actor) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("UPDATE ACTOR SET first_name = ?, last_name = ?,"
					+ " gender = ?, birthdate = ?, rating = ?, biography = ? WHERE id = ? ");
			ps.setString(1, actor.getFirstName());
			ps.setString(2, actor.getLastName());
			ps.setBoolean(3, actor.isGender());
			ps.setDate(4, new java.sql.Date(actor.getBirthdate().getTime()));
			ps.setInt(5, actor.getRating());
			ps.setString(6, actor.getBiography());
			ps.setInt(7, actor.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void updateAdmin(Admin admin) {
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("UPDATE ADMIN SET name = ?, password = ? WHERE id = ?");
			ps.setString(1, admin.getName());
			ps.setString(2, admin.getPassword());
			ps.setInt(3, admin.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateDirector(Director director) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement(
					"UPDATE DIRECTOR SET first_name = ?, last_name = ?," + " gender = ?, birthdate = ? WHERE id = ? ");
			ps.setString(1, director.getFirstName());
			ps.setString(2, director.getLastName());
			ps.setBoolean(3, director.isGender());
			ps.setDate(4, new java.sql.Date(director.getBirthdate().getTime()));
			ps.setInt(5, director.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void updateGenre(Genre genre) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("UPDATE GENRE SET name = ? WHERE id = ? ");
			ps.setString(1, genre.getName());
			ps.setInt(2, genre.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void updateMovie(Movie movie) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement(
					"UPDATE MOVIE SET title = ?, movie_year = ?, rating = ?, description = ?, director_id = ? WHERE id = ? ");
			ps.setString(1, movie.getTitle());
			ps.setInt(2, movie.getYear());
			ps.setInt(3, movie.getRating());
			ps.setString(4, movie.getDescription());
			ps.setInt(5, movie.getDirector_id());
			ps.setInt(6, movie.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public Actor getActor(int actorId) {

		Actor actor = new Actor();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM ACTOR WHERE id = ?");
			ps.setInt(1, actorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				actor.setId(rs.getInt("id"));
				actor.setFirstName(rs.getString("first_name"));
				actor.setLastName(rs.getString("last_name"));
				actor.setGender(rs.getBoolean("gender"));
				actor.setBirthdate(rs.getDate("birthdate"));
				actor.setRating(rs.getInt("rating"));
				actor.setBiography(rs.getString("biography"));
			}
			return actor;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}
	}

	public Director getDirector(int directorId) {

		Director director = new Director();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM DIRECTOR WHERE id = ?");
			ps.setInt(1, directorId);
			rs = ps.executeQuery();
			while (rs.next()) {
				director.setId(rs.getInt("id"));
				director.setFirstName(rs.getString("first_name"));
				director.setLastName(rs.getString("last_name"));
				director.setGender(rs.getBoolean("gender"));
				director.setBirthdate(rs.getDate("birthdate"));
			}
			return director;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}
	}

	public Genre getGenre(int genreId) {

		Genre genre = new Genre();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("SELECT * FROM GENRE WHERE id = ?");
			ps.setInt(1, genreId);
			rs = ps.executeQuery();
			while (rs.next()) {
				genre.setId(rs.getInt("id"));
				genre.setName(rs.getString("name"));
			}
			return genre;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}
	}

	public Movie getMovie(int movieId) {

		Movie movie = new Movie();
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement(
					"SELECT * FROM MOVIE m INNER JOIN DIRECTOR d ON m.director_id = d.id WHERE m.id = ?");
			ps.setInt(1, movieId);
			rs = ps.executeQuery();
			while (rs.next()) {
				movie.setId(rs.getInt("m.id"));
				movie.setTitle(rs.getString("m.title"));
				movie.setYear(rs.getInt("m.movie_year"));
				movie.setDescription(rs.getString("m.description"));
				movie.setRating(rs.getInt("m.rating"));
				movie.setDirector_id(rs.getInt("m.director_id"));
				movie.setDirector_name(rs.getString("d.first_name") + " " + rs.getString("d.last_name"));
			}
			return movie;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			disconnect();
		}
	}

	public void deleteActor(int actorId) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("DELETE FROM ACTOR WHERE id = ?");
			ps.setInt(1, actorId);
			ps.execute();
		} catch (SQLException e) { // Will be handled Cascade execption in
									// MySQLIntegrityConstraintViolationException
									// web.xml
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void deleteAdmin(int adminId) {
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("DELETE FROM ADMIN WHERE id = ?");
			ps.setInt(1, adminId);
			ps.execute();
		} catch (SQLException e) {
			// TODO: handle exception
		}
	}

	public void deleteDirector(int directorId) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("DELETE FROM DIRECTOR WHERE id = ?");
			ps.setInt(1, directorId);
			ps.execute();
		} catch (SQLException e) { // Will be handled Cascade execption in
									// web.xml
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void deleteGenre(int genreId) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("DELETE FROM GENRE WHERE id = ?");
			ps.setInt(1, genreId);
			ps.execute();
		} catch (SQLException e) { // Will be handled Cascade execption in
									// web.xml
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void deleteMovie(int movieId) {

		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("DELETE FROM MOVIE WHERE id = ?");
			ps.setInt(1, movieId);
			ps.execute();
		} catch (SQLException e) { // Will be handled Cascade execption in
									// web.xml
			e.printStackTrace();
		} finally {
			disconnect();
		}
	}

	public void deleteRelationsOfMovie(int movieId) {
		if (!isConnected) {
			connect();
		}
		try {
			ps = conn.prepareStatement("DELETE FROM MOVIE_ACTOR_MAP WHERE movie_id = ?");
			ps.setInt(1, movieId);
			ps.execute();
			ps = conn.prepareStatement("DELETE FROM MOVIE_GENRE_MAP WHERE movie_id = ?");
			ps.setInt(1, movieId);
			ps.execute();
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			disconnect();
		}
	}
}
